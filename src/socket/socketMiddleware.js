import {AppConfig} from '../AppConfig';
import {actions, WS_CONNECT, WS_DISCONNECT, WS_SEND} from './actions';

const socketMiddleware = () => {
    let webWorker = null;

    console.log('socketMiddleware start');

    const wsUrl = AppConfig.urlWebSocket;

    const onMessage = (store) => (event) => {
        //console.log('receiving server message', event);

        const payload = event.data?.response || event.data; //event.data is on errors
        store.dispatch(actions.wsReceived(payload));
    };

    return (store) => (next) => (action) => {
        //console.log('socketMiddleware store', action.type);
        switch (action.type) {
        case WS_CONNECT:
            if (webWorker !== null) {
                webWorker.postMessage({task: 'disconnect'});
            }
            webWorker = new Worker('./webWorker.js');
            //console.log('socketMiddleware webWorker', wsUrl);

            webWorker.postMessage({task: 'connect', url: wsUrl,});

            webWorker.onmessage = onMessage(store);
            break;

        case WS_DISCONNECT:
            if (webWorker !== null) {
                webWorker.postMessage({task: 'disconnect'});
            }
            webWorker = null;
            //console.log('websocket closed');
            break;

        case WS_SEND:
            //console.log('sending a message', action.payload);
            try {
                webWorker.postMessage(action.payload);
            }
            catch (error) {
                setTimeout(() => {
                    const windowLocation = '' + window.location;
                    webWorker && webWorker.postMessage(windowLocation, action.payload);
                }, 1500);
            }
            break;

        default:
            return next(action);
        }
    };
};

export default socketMiddleware();
