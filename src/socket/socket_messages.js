import {ce, WsEvt} from '../library/storage';
import CryptoJS from 'crypto-js';

function msgSend(evt, payload, params)
{
    return {
        msg: {
            event: ce(evt),
            payload,
        },
        task: 'send',
    };
}


export const logoutMsg = () => {
    return msgSend(WsEvt.Player_Logout, {});
};

export const loginMsg = (formData) => {
    const modifiedData = {
        cUsername: formData.username,
        cPassword: CryptoJS.MD5(formData.password).toString()

    }
    return msgSend(WsEvt.Player_Login, modifiedData);
};

export const bigWinners = () => {
    return msgSend(WsEvt.Big_Winners, {})
}

export const winnerBig = () => {
    return msgSend(WsEvt.Winner_big, {})
}

export const initConnection = (payload) => {
    return msgSend(WsEvt.Connection_Init, payload);
};