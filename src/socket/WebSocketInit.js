import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import Loader from '../components/Loader';
import {actions} from './actions';
import {storage, StoreKey} from '../library/storage';
import {initConnection} from './socket_messages';
import {AppConfig} from '../AppConfig';

const WebSocketInit = ({children}) => {
    const dispatch = useDispatch();
    const token = useSelector(({data}) => data?.token);
    const connected = useSelector(({data}) => data?.connected);
    const init = useSelector(({data}) => data && data?.init);
    const userTokenFromLS = storage.load(StoreKey.Token);

    useEffect(() => {
        //connect to server
        dispatch(actions.wsConnect());
    }, [dispatch]);

    useEffect(() => {
        if (!connected) {
            return;
        }
        if (init === null || init === undefined) {
            //receive config
            const payload = {
                domainsetId: AppConfig.domainSet,
                language_alpha2: 'en',
            };

            if (token) {
                payload.token = token;
            }
            else if (userTokenFromLS) {
                payload.token = userTokenFromLS;
            }

            dispatch(actions.wsSend(initConnection(payload)));
        }
    }, [init, connected, dispatch, userTokenFromLS, token]);

    return (init === null || init === undefined
            ? <div className="flex items-center mb-48 w-full"><Loader text={'loading'}/></div>
            : <>{children}</>);
};

export default WebSocketInit;