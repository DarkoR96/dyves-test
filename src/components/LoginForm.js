import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@mui/material/Button';
import React, {useState } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import * as yup from 'yup';
import TextField from '@mui/material/TextField';
import { actions } from '../socket/actions';
import { loginMsg } from '../socket/socket_messages';
import { makeStyles } from '@material-ui/core';
import Toast from './Toast';

const useStyles = makeStyles(() => ({
    root: {
        height: '100vh',
        display: 'flex',
        placeItems: 'center',
    },
    loginForm: {
        width: 330,
        margin: '0 auto',
        display: 'flex',
        flexDirection: 'column',
        background: 'white',
        padding: 30,
        gap: 30

    },
    items: {
        margin: '10px !important',
    },

}));

const schema = yup.object().shape({
    username: yup.string().required('You must enter a username'),
    password: yup.string().required('Please enter your password.'),
});

function LoginForm() {
    const classes = useStyles();
    const dispatch = useDispatch()

    const { handleSubmit, register, formState: { errors } } = useForm({
        resolver: yupResolver(schema),
    });


    const onSubmit = (data) => handleLogin(data)
    const onError = (error) => console.log(error) 

    const handleLogin = (formData) => {
        dispatch(actions.wsSend(loginMsg(formData)));
    }

    const [open, setOpen] = useState(true)

    const msg = useSelector(state => state.data)

    return <div className={classes.root}>

        <form onSubmit={handleSubmit(onSubmit, onError)} className={classes.loginForm}>
            <TextField label="Username*" error={errors?.username} helperText={errors?.username?.message} {...register("username")}/>
            <TextField label="Password*" error={errors?.password} helperText={errors?.password?.message} {...register("password")} type="password" />
            <Button variant="contained" type="submit" onClick={() => setOpen(true)}>Log in</Button>
        </form>
    
        {msg?.error && <Toast open={open} message={msg.error} severity={'error'} onClose={() => setOpen(false)} />}
    </div>;
    
}



export default LoginForm;