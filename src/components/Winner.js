import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles, Typography } from '@material-ui/core';
import { bigWinners, winnerBig } from '../socket/socket_messages';
import { actions } from '../socket/actions';

const useStyles = makeStyles(() => ({
    bigWinner: {
        width: 330,
        background: '#38414a',
        marginTop: 10,
        textAlign: 'center',
        borderRadius: 10,
        paddingBottom: 10
    },
    list: {
        listStyleType: 'none',
        padding: 0,
        margin: 0,
    },
    listItem: {
        display: 'grid',
        gridTemplateColumns: '1fr 1fr 1fr',
        color: '#afb7d0',
        padding: 10,
    },
    listHeader: {
        display: 'flex',
        justifyContent: 'space-between',
        color: '#fff',
        padding: '15px 10px',
    },
    playerName: {
        color: '#fff',
        textAlign: 'left',
        fontWeight: 600,
    },
    amount: {
        color: '#fafa01',
        textAlign: 'right',
        fontWeight: 600,
    }
}));


const formatMoney = (amount, currency) => {

    const currency_map = {
        "EUR": "€",
        "USD": "$",
    }
    
    return `${currency_map[currency] || currency} ${parseFloat(amount / 100).toFixed(Math.abs(2))}`
};

const Winner = () => {
    const classes = useStyles();
    const dispatch = useDispatch()


    const { winners } = useSelector(state => state.data)


    useEffect(() => {
        if (!winners.length) {
            dispatch(actions.wsSend(bigWinners()))
        }
    }, [])


    useEffect(() => {
        dispatch(actions.wsSend(winnerBig()))   
    }, [winners.length])


    return <div className={classes.bigWinner}>
        <ul className={classes.list}>
            <li className={classes.listHeader}><span>USERNAME</span> <span /> <span>PAYOUT</span></li>
            {winners?.sort((a, b) => b.ts - a.ts).slice(0, 10).map((winner, idx) => (
                <li key={idx} className={classes.listItem}><span className={classes.playerName}>{winner.player}</span> has won <span className={classes.amount}>{formatMoney(winner.amount, winner.currency)}</span></li>
            ))}
        </ul>
    </div>;
};

export default Winner;