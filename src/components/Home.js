import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import LoginForm from './LoginForm';
import Button from '@mui/material/Button';
import {actions} from '../socket/actions';
import {logoutMsg} from '../socket/socket_messages';
import {AppBar, makeStyles, Toolbar, Typography} from '@material-ui/core';
import IconButton from '@mui/material/IconButton';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import Winner from './Winner';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    logout: {
        color: 'white !important',
        textDecoration: 'underline !important',
    },
}));

function Home()
{
    const dispatch = useDispatch();
    const loggedIn = useSelector(({data}) => data?.loggedIn);
    const classes = useStyles();

    const handleLogout = () => {
        dispatch(actions.wsSend(logoutMsg()));
    };

    if (!loggedIn) {
        return <LoginForm/>;
    }

    return <div className={classes.root}>
        <AppBar position="static">
            <Toolbar>
                <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                    <AccountCircleOutlinedIcon/>
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                    Welcome!
                </Typography>
                <Button className={classes.logout} onClick={handleLogout}>
                    Logout
                </Button>
            </Toolbar>
        </AppBar>
        <Winner/>
    </div>;
}

export default Home;