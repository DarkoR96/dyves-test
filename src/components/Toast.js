import React from 'react';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/lab/Alert';
import ReactHtmlParser from 'react-html-parser';

const Toast = ({
    open,
    autoHideDuration = 3000,
    onClose,
    severity,
    message,
    ...snackbarProps
}) => {
    return (
        <>
    <Snackbar
        anchorOrigin={{
            horizontal: 'center',
            vertical: 'bottom',
        }}
        open={open}
        autoHideDuration={autoHideDuration}
        transitionDuration={300}
        onClose={onClose}
        message={message}
        {...snackbarProps}>
        <Alert severity={severity}
               onClose={onClose}
               style={{
                   fontSize: '1rem',
                   maxWidth: 330,
                   overflowWrap: 'anywhere',
               }}>
            {ReactHtmlParser(message)}
        </Alert>
    </Snackbar>
    </>);
};

export default Toast;
