import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import {Provider} from 'react-redux';
import store from './store';
import WebSocketInit from './socket/WebSocketInit';

ReactDOM.render(<Provider store={store}>
    <WebSocketInit>
        <App/>
    </WebSocketInit>
</Provider>, document.getElementById('root'));