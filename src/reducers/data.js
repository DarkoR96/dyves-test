import { storage, StoreKey, WsEvt } from '../library/storage';
import { DELETE_DATA, WS_RECEIVED } from '../socket/actions';

const initState = {
    init: null,
    connected: false,
    loggedIn: false,
    loading: {},
    error: null,
    winners: [],
};

export default (state = initState, action) => {
    switch (action.type) {
        case WS_RECEIVED:
            if (action.payload.type === 'socket') {
                return {
                    ...(action.payload.status === 'close'
                        ? initState
                        : state),
                    connected: action.payload.status === 'open',
                };
            }

            return wsEvent(action, state, initState);

        case DELETE_DATA:
            if (state[action.key]) {
                let nextState = { ...state };
                delete nextState[action.key];
                return nextState;
            }
            return state;

        default:
            return state;
    }
};

export function wsEvent(action, state, initState) {
    const event = action.payload?.event;

    if (event) {
        const eventName = event.substr(4);

        if (event.substr(0, 4) === '/se/') {
            return seEvent(eventName, action, state, initState);
        }

        if (event.substr(0, 4) === '/ie/') {
            return ieEvent(eventName, action, state, initState);
        }

        console.error('unknown event:', event);

        return state;
    }
}

function seEvent(eventName, action, state, initState) {
    const actionPayload = action.payload;
    const payload = actionPayload?.payload;
    const status = Number.parseInt(actionPayload?.status);
    const statusOk = status === 200;

    switch (eventName) {

        case WsEvt.Player_Login:
            if (statusOk) {
                storage.save(StoreKey.Token, payload.token);
                return {
                    ...state,
                    loggedIn: true,
                };
            }
            else {
                storage.remove(StoreKey.Token);

                return {
                    ...state,
                    error: payload.message,
                };
            }

        case WsEvt.Big_Winners:

            return {
                ...state,
                winners: payload
            };


        case WsEvt.Player_Logout:
            storage.remove(StoreKey.Token);
            return {
                ...initState,
            };

        case WsEvt.Connection_Init:
            const token = payload?.player?.token;
            const loggedIn = !!token;
            if (loggedIn) {
                storage.save(StoreKey.Token, token);
            }
            else {
                storage.remove(StoreKey.Token);
            }

            return {
                ...state,
                init: payload,
                loggedIn: loggedIn,
            };

        default:
            console.warn('unknown se event', action);
            return state;
    }
}

function ieEvent(eventName, action, state) {
    const actionPayload = action.payload;
    const payload = actionPayload?.payload;

    switch (eventName) {
        case WsEvt.Connection_Info:
            return state;

        case WsEvt.Winner_big:
            return {
                ...state,
                winners: [...payload, ...state.winners]
            };
        default:
            console.warn('unknown ie event', action);
            return {
                ...state,
                [eventName]: payload,
            };
    }
}