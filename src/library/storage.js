export function se(key)
{
    return '/se/' + key;
}

export function ce(key)
{
    return '/ce/' + key;
}

export function ie(key)
{
    return '/ie/' + key;
}

export const WsEvt = {
    Connection_Init: 'connection/init3',
    Connection_Info: 'connection/info',
    Player_Login: 'player/login',
    Player_Logout: 'player/logout',
    Big_Winners: 'casinoGames/winner/bigSub',
    Winner_big: 'casinoGames/winner/big'
};

export const StoreKey = {
    Token: 'token',
};

class Storage
{
    save(key, value)
    {
        try {
            localStorage.setItem(key, JSON.stringify(value));
        }
        catch (error) {
            // Error saving data
            localStorage.removeItem(key);
        }
    }

    load(key)
    {
        try {
            const data = localStorage.getItem(key);
            if (!data) {
                return null;
            }
            // We have data!!
            return JSON.parse(data);
        }
        catch (error) {
            window.localStorage.removeItem(key);
        }
    }

    remove(key)
    {
        try {
            localStorage.removeItem(key);
        }
        catch (error) {
            console.error('storage', error);
        }
    }

    clear()
    {
        localStorage.clear();
    }
}

export const storage = new Storage();
